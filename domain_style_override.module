<?php

/**
 * @file - Provides per domain style override functionality
 */

/**
 * Implements hook_preprocess_html()
 */
function domain_style_override_preprocess_html(&$variables) {
  global $_domain, $theme_path;

  if (!empty($_domain)) {
    //provide body tags classes for simple CSS overrides
    $variables['classes_array'][] = drupal_html_class('domain-id-' . $_domain['domain_id']); //shouldn't change
    $variables['classes_array'][] = drupal_html_class('domain-name-' . $_domain['sitename']); //could be useful

    // Scan DSO themes for css file per domain
    // pattern: domain-css-DOMAINID.css
    foreach (domain_style_override_get_themes() as $theme => $path) {
      $domain_pattern = 'domain-css-' . $_domain['domain_id'] . '.css';
      $domain_css = file_scan_directory($path, $domain_pattern, array('key' => 'name'));
      if (!empty($domain_css)) {
        drupal_add_css($domain_style, array('group' => CSS_THEME, 'weight' => 100));
      }
    }
  }
}

/**
 * Implements hook_preprocess_page()
 *
 * Scan DSO themes for logo per domain
 * pattern: domain-logo-DOMAINID.png
 */
function domain_style_override_preprocess_page(&$variables) {
  global $_domain;

  // is domain access enabled scan for per domain logo file
  // pattern = logo_domain-DOMAINID.png
  if (!empty($_domain)) {
    foreach (domain_style_override_get_themes() as $theme => $path) {
      $domain_logo = $path . '/images/logo_domain-' . $_domain['domain_id'] . '.png';
      if (file_exists($domain_logo)) {
        $variables['logo'] = '/' . $domain_logo;
      }
    }
  }
}

/**
 * HOOK_html_head_alter
 *
 * Scan DSO themes for favicon per domain
 * pattern: domain-favicon-DOMAINID.png
 */
function domain_style_override_html_head_alter(&$head_elements) {
  global $_domain;

  if (!empty($_domain) && $_domain['domain_id'] > 0) {
    foreach (domain_style_override_get_themes() as $theme => $path) {
      $domain_favicon = $path . '/images/favicon_domain-' . $_domain['domain_id'] . '.png';
      if (file_exists($domain_favicon)) {
        foreach ($head_elements as $key => $element) {
          if (!empty($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortcut icon') {
            $head_elements[$key]['#attributes']['href'] = '/' . $domain_favicon;
            $head_elements[$key]['#attributes']['type'] = 'image/png';
          }
        }
      }
    }
  }
}

/**
 * Return path to all DSO themes
 *
 * @todo
 * - add admin page for selecting & ordering themes
 * - API hook
 */
function domain_style_override_get_themes() {
  global $theme_path, $theme_key;

  $themes = array();
  $themes[$theme_path] = $theme_key;
  return $themes;
}
